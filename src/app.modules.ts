import {
    existsSync, 
    readdirSync, 
    readFileSync, 
    statSync,
} from 'fs';
import * as path from 'path'; 

 export default () =>
 {
    const imports = [];
    const dir = `${__dirname}/modules`;
    const files = readdirSync(dir);
    for (const file of files) {
        const stat = statSync(path.join(dir, file));
        if (stat.isDirectory()) {
            if (existsSync(path.join(dir, file, '/index.ts'))) {
                const module = require(path.join(dir, file, '/index.ts'));
                imports.push(module.default());
            }
        }
    } 
    return imports
 } 