import { EXTERNAL_TYPE } from "@common/interface";
import { UserLandEntity } from "@users/entities";
import { UserEntity } from "@users/entities/user.entity"
import { Role } from "@users/models/roles.enum";
import { Exclude } from "class-transformer";
import { FileEntity } from "src/files/entities/file.entity";

export class UserResponse implements UserEntity {
    constructor(user: UserEntity) {
        Object.assign(this, user);
    }
    id: number; 
    email: string
    user_email?: string
    roles?: Role[]
    
    @Exclude()
    password: string
 
    user_descr?: string
    phone?: string
    avatar?: string
    avatar_name?: string
    avatar_id?: string
    first_name?: string
    last_name?: string
    display_name?: string
    external?: EXTERNAL_TYPE
    is_external?: boolean
    externalId ?: string
    is_blocked?: boolean
    files?: FileEntity[];
    
    @Exclude()
    userLand?: UserLandEntity[];
    
}