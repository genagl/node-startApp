import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { UserEntity } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { UserLandEntity } from './entities';
import { UserPaging } from './dto/user-paging.dto';

@Injectable()
export class UsersService { 
  constructor(
    @InjectRepository(UserEntity)
    private repository: Repository<UserEntity>,
    @InjectRepository(UserLandEntity)
    private readonly userLandRepository: Repository<UserLandEntity>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  async findByEmail(email: string, land_id?: number | null ) { 
    let user: UserEntity = await this.repository.findOneBy({ email, });
    if(land_id) {
      const options: FindOneOptions<UserLandEntity> = {
        relations: {
            land: true,
        },
        where: { land: {id: land_id} }
      };
      const userLand: UserLandEntity = await this.userLandRepository.findOne(options);
      user = {
        ...user,
        is_blocked: userLand.is_blocked,
        roles: userLand.roles
      }
    }
    return user;
  }

  async findById(id: number, land_id?: number ) {
    let user: UserEntity = await this.repository.findOneBy({ id, });
    if(land_id) {
      const options: FindOneOptions<UserLandEntity> = {
        relations: {
            land: true,
        },
        where: { land: {id: land_id} }
      };
      const userLand: UserLandEntity = await this.userLandRepository.findOne(options);
      user = {
        ...user,
        is_blocked: !!userLand?.is_blocked,
        roles: userLand?.roles
      }
    }
    return user;
  }

  async create(dto: CreateUserDto) {
    const user: UserEntity = await this.repository.save(dto);
    await this.cacheManager.set(user.id.toString(), user);
    await this.cacheManager.set(user.email, user);
    return user;
  }

  async update(id:number, dto: UpdateUserDto, land_id?: number ): Promise<any> {
    const user = await this.repository.findOneBy({ id, });
    await this.cacheManager.set(user.id.toString(), {...user , ...dto});
    await this.cacheManager.set(user.email, {...user , ...dto});
    return await this.repository.update(id, {...user , ...dto});
  } 

  async findAll( paging? : UserPaging, land_id?: number ): Promise<UserEntity[]> {
    const qb = this.repository.createQueryBuilder('user');
    return qb.getMany();
  }

  async remove( ids: string, land_id?: number ) {
    const idsArray = ids.split(',');
    const qb = this.repository.createQueryBuilder('file');
    qb.where('id IN (:...ids)', {
      ids: idsArray, 
    });
    qb.softDelete().execute();
    // await Promise.all([this.cacheManager.del(id), this.cacheManager.del(user.email)]);
    return true;
  }

  async addAvatar ( userId: number, data: any): Promise<UserEntity> {
    const user = await this.repository.findOneBy({ id: userId, });
    this.repository.update( 
      userId, 
      { 
        ...user, 
        avatar_name: `${data.filename}` 
      } 
    );
    return await this.repository.findOneBy({ id: userId, });
  }
}
