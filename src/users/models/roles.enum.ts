import { registerEnumType } from "@nestjs/graphql";

export enum Role {
    Administrator="Administrator",
    Editor="Editor",
    Track_moderator="Track_moderator",
    Author="Author",
    Expert="Expert",
    Tutor="Tutor",
    Project_member="Project_member",
    User="User",
    Subscriber="Subscriber" 
}

registerEnumType(Role, {name: 'Role'})

export const RoleResolver: Record<keyof typeof Role, any> = {
    Administrator: "Administrator",
    Editor: "Editor",
    Track_moderator: "Track_moderator",
    Author: "Author",
    Expert: "Expert",
    Tutor: "Tutor",
    Project_member: "Project_member",
    User: "User",
    Subscriber: "Subscriber" 
};