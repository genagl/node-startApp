import { MetaFilter } from "@common/dto/meta-filter.dto";
import { Field, InputType, Int } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";

@InputType("UserPaging")
export class UserPaging {

    @ApiProperty( ) 
    @Field(() => Int, { description: "count", nullable: true  }) 
    count?: number | null;

    @ApiProperty( ) 
    @Field(() => Int, { description: "offset", nullable: true  }) 
    offset?: number | null;

    @ApiProperty( ) 
    @Field(() => Int, { description: "meta relation ( OR | AND)", nullable: true  }) 
    meta_relation?: string | null;

    @ApiProperty( ) 
    @Field(() => Int, { description: "order (DESC | ASC)", nullable: true  }) 
    order?: string | null;

    @ApiProperty( ) 
    @Field(() => [MetaFilter], { description: "MetaFilter", nullable: true  }) 
    metas?: MetaFilter[] | null;
    
    @ApiProperty( ) 
    @Field(() => Boolean, { description: "is_admin", nullable: true  }) 
    is_admin?: boolean | null;

    @ApiProperty( ) 
    @Field(() => Int, { description: "search", nullable: true  }) 
    search?: string | null; 
}