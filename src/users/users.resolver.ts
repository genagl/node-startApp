import { NotFoundException, UseGuards, UseInterceptors } from '@nestjs/common';
import { Args, Int, Query, Mutation, Resolver, ID } from '@nestjs/graphql';  
import { UsersService } from './users.service'; 
import { UserEntity } from './entities/user.entity';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { Role } from './models/roles.enum';
import { RolesGuard } from '../auth/guards/roles.guard';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateURLAvatarInterceptor } from './interceptors';
import { UserRolesNotNullableInterceptor } from './interceptors/user-roles-not-nullable.interceptor';
import { UserPaging } from './dto/user-paging.dto';

@Resolver( of => UserEntity )
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @UseInterceptors(UpdateURLAvatarInterceptor)
  @UseInterceptors(UserRolesNotNullableInterceptor)
  @Query( () => UserEntity, { name: 'getUser',  nullable: true, description: "Get single User" } )
  async user(
    @Args('id', { type: () => Int }) id: number,
    @Args('land_id', {type: () => Int, nullable: true }, ) land_id: number, 
  ): Promise<UserEntity> {
    const user: UserEntity = await ( this.usersService.findById( id, land_id ) ) 
    return user;
  }
  
  @UseInterceptors(UpdateURLAvatarInterceptor)
  @UseInterceptors(UserRolesNotNullableInterceptor)
  @Query(() => [UserEntity], { nullable: 'items', description: `Get list of Users` })
  //@UseGuards( JwtAuthGuard, RolesGuard )
  async getUsers(
    @Args('land_id', {type: () => ID, nullable: true }, ) land_id: number | string, 
    @Args("paging",  {type: () => UserPaging, nullable: true }, ) paging: UserPaging
  ): Promise<UserEntity[]> {
    const users: UserEntity[] = await ( this.usersService.findAll( paging, parseInt(land_id.toString()) ) )
    return users;
  }

  @UseInterceptors(UpdateURLAvatarInterceptor)
  @UseInterceptors(UserRolesNotNullableInterceptor)
  @Mutation( () => UserEntity, {  name: 'updateUser', description: `Update single User`} )
  @UseGuards( JwtAuthGuard, RolesGuard )
  update( id: number, updateUserDto: CreateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }
}
