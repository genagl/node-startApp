import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { GqlContextType, GqlExecutionContext } from "@nestjs/graphql";
import { Role } from "@users/models/roles.enum";
import { Request } from 'express';
import { map, Observable } from "rxjs";


@Injectable()
export class UserRolesNotNullableInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        let req: Request;
        if (context.getType<GqlContextType>() === 'graphql') {
            const gqlContext: GqlExecutionContext = GqlExecutionContext.create(context);
            req = gqlContext.getContext().req;
        }
        else {
            const httpContext = context.switchToHttp();
            req = httpContext.getRequest();
        }
        return next 
            .handle()
            .pipe(map((value: any) => recursivelyFillRoles(value, req) ))
    }

}

const recursivelyFillRoles = (value: any, request: Request): any => {
    if (Array.isArray(value)) {
        return value.map(v => recursivelyFillRoles(v, request));
    }
    if (!value?.roles || (Array.isArray(value?.roles) && !value?.roles.length)) { 
        return {
            ...value,
            roles: [Role.User]
        };
    } 
    return value; 
}