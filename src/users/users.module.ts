import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { UsersResolver } from './users.resolver';
import { UserLandEntity } from './entities';

@Module({
  controllers: [UsersController],
  providers: [UsersService, UsersResolver],
  imports: [TypeOrmModule.forFeature([UserEntity, UserLandEntity])],
  exports: [UsersService], 
})
export class UsersModule {}
