import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UseInterceptors, UploadedFile, ParseFilePipe, MaxFileSizeValidator, Req, SetMetadata, ClassSerializerInterceptor } from '@nestjs/common';
import {HttpStatus, ParseIntPipe} from '@nestjs/common'
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger'; 
import { UserId } from './decorators/user-id.decorator';
import { UserEntity } from './entities/user.entity';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard'; 
import { avatarStorage } from 'src/files/storage';
import { FileInterceptor } from '@nestjs/platform-express';
import FileUploadDto from './dto/avatar-upload.dto';
import { Role } from './models/roles.enum';
import { HostUrl } from '@common/decorators';
import { RolesGuard } from '@auth/guards/roles.guard';
import { UserResponse } from './responses';
import { UpdateURLAvatarInterceptor } from './interceptors';

@Controller('users') 
@ApiTags("users")
//@ApiBearerAuth() 
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('/me') 
  @UseGuards(JwtAuthGuard) 
  @ApiBearerAuth()
  @ApiOperation({ summary: "Get current User data" })
  async getMe(@UserId() id: number) {
    const user = await this.usersService.findById(id);
    return new UserResponse(user);
  } 
  
  @UseInterceptors(ClassSerializerInterceptor)
  @Post()   
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ summary: "Create new User" })
  @UseGuards( JwtAuthGuard, RolesGuard )
  @SetMetadata('roles', [ Role.Administrator, Role.Author ])
  @ApiBearerAuth()
  async create(  
    @Body() 
    createUserDto: CreateUserDto,    
  ) {   
    console.log( createUserDto )
    const user = await this.usersService.create(createUserDto);
    return new UserResponse(user);
  }

  
  @UseInterceptors(ClassSerializerInterceptor)
  @Post('/avatar') 
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @UseInterceptors( FileInterceptor('file', { storage: avatarStorage, }), )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'A new avatar for the user',
    type: FileUploadDto,
  })
  async addAvatar( 
    @UserId() userId: number, 
    @UploadedFile() file: Express.Multer.File,
    @HostUrl() hostURL: string
  ) {
    console.log( file );
    const user = await this.usersService.addAvatar(userId, {
      destination: hostURL + "/avatars",
      filename: file.filename,
      mimetype: file.mimetype
    });
    return new UserResponse(user);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(UpdateURLAvatarInterceptor)
  @Get()
  @UseGuards( JwtAuthGuard, RolesGuard )
  @SetMetadata('roles', [ Role.Administrator, Role.Author ])
  @ApiOperation({ summary: "Get Users list" })
  async findAll() {
    const users = await this.usersService.findAll();
    return users.map(user => new UserResponse(user));
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(UpdateURLAvatarInterceptor)
  @Get(':id')
  @ApiOperation({ summary: "Get single User by specified id" })
  async findOne(@Param('id') id: number) {
    const user = await this.usersService.findById( +id );
    return new UserResponse(user);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(UpdateURLAvatarInterceptor)
  @Patch(':id')
  @UseGuards( JwtAuthGuard, RolesGuard )
  //@SetMetadata('roles', [ Role.ADMIN, Role.AUTHOR ])
  @ApiBearerAuth()
  @ApiOperation({ summary: "Updates a User with specified id" })
  @ApiParam({ name: "id", required: true, description: "User identifier" })
  @ApiConsumes('multipart/form-data')
  @ApiResponse({ status: HttpStatus.OK, description: "Success", type: UserEntity })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "Bad Request" })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: "Unauthorized" })
  async update(@Param('id', new ParseIntPipe()) id: number, @Body() userDto: UpdateUserDto) {
    this.usersService.update(+id, userDto); 
    const user = await this.usersService.findById( +id );
    return new UserResponse(user);
  }

  @Delete(':id')
  @UseGuards( JwtAuthGuard, RolesGuard )
  @SetMetadata('roles', [ Role.Administrator, Role.Author ])
  @ApiBearerAuth()
  @ApiOperation({ summary: "Remove User by ID" })
  async remove(@Param('id') id: string) {
    const user = await this.usersService.remove( id );
    return user;
  } 
}
