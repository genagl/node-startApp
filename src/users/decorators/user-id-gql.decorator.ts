import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const UserIdGql = createParamDecorator(
  (_: unknown, context: ExecutionContext): number | null => {
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;
    //const request = context.switchToHttp().getRequest();
    return req.user?.id ? Number(req.user.id) : null;
  },
);
