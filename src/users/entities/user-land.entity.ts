import { LandEntity } from "@lands/entities";
import { Field, Int, ObjectType } from "@nestjs/graphql";
import { Role } from "@users/models/roles.enum";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user.entity";


@Entity("user-land")
@ObjectType("UserLand", { description: 'user-land' })
export class UserLandEntity {

    @PrimaryGeneratedColumn()
    @Field( () => Int ) 
    public userLandId: number;
    
    @Column( { type: "enum", enum: Role, array: true, default: [Role.User]  } ) 
    @Field(() => [Role], { description: "List of roles" }) 
    public roles: Role[]; 
    
    @Column({ nullable: true })
    @Field({ nullable: true, description: 'User is blocked by admin?', defaultValue: false })
    public is_blocked?: boolean;  

    @ManyToOne(() => LandEntity, (user) => user.userLand)
    public land: LandEntity;
    
    @ManyToOne(() => UserEntity, (land) => land.userLand)
    public user: UserEntity;

}