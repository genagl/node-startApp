import { Module } from "@nestjs/common";
import { GqlTestProvider } from "./gqlTest.provider";

@Module({
    providers: [GqlTestProvider],
})
export class GqlTestModule {}