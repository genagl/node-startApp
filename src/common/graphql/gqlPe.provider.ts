import { Mutation, Query, Resolver } from "@nestjs/graphql";  
import { OptionEntity } from "../options/entities/option.entity";
import { OptionsService } from "../options/options.service";
import { ID } from "../interface";


@Resolver()
export class GqlPEProvider {
    constructor(private readonly _optionsService: OptionsService) {}

    @Query(() => OptionEntity, {name: "getOptions"})
    getOptions( id: number ) : Promise<OptionEntity> {
        return this._optionsService.findOne( id );
    }

    @Mutation(() => OptionEntity, {name: "changeOptions"})
    changeOptions() : Promise<OptionEntity> {
        return new Promise(() => {
             return {

             }
        });
    }
}