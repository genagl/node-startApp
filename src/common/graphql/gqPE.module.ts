import { Module } from "@nestjs/common";
import { GqlPEProvider } from "./gqlPe.provider";

@Module({ providers: [ GqlPEProvider ], })
export class GqlPEModule {

}