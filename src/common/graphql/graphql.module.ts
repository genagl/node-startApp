import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";
import { Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import GraphQLJSON from 'graphql-type-json';
import { GqlTestModule } from "./gqlTest.module";
import { formatError } from "./graphql-error.format"
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';
import { DirectiveLocation, GraphQLDirective, GraphQLSchema } from "graphql";
import { upperDirectiveTransformer } from "../directives/upper-case.directive";
import { ConfigService } from "@nestjs/config";

@Module({
    imports: [
        GqlTestModule,
        GraphQLModule.forRootAsync<ApolloDriverConfig>({
            driver: ApolloDriver, 
            imports:[],
            inject: [ConfigService],
            useFactory: (configService: ConfigService):
            | Omit<ApolloDriverConfig, 'driver'>
            | (Promise<Omit<ApolloDriverConfig, 'driver'>> & { uploads: boolean }) => {
                return {
                  uploads: false,
                  fieldResolverEnhancers: ['interceptors'],
                  resolvers: { JSON: GraphQLJSON },
                  autoSchemaFile: 'graphql-schema.gql',
                  transformSchema: (schema: GraphQLSchema) => upperDirectiveTransformer(schema, 'pe'),
                  installSubscriptionHandlers: true, 
                  sortSchema: true,
                  playground: false,
                  ...( configService.get("NODE_ENV") !== "production" && {
                    plugins: [ApolloServerPluginLandingPageLocalDefault()],
                  }),
                  context: ({ req }) => ({ req }),
                  cache: 'bounded',
                  buildSchemaOptions: {
                    noDuplicatedFields: true,
                    directives: [
                      new GraphQLDirective({
                        name: 'pe',
                        locations: [DirectiveLocation.SCHEMA],
                      }),
                    ],
                  },
                  formatError,
                }
            }
            // resolvers: { JSON: GraphQLJSON },
            // autoSchemaFile: 'schema.gql', 
            // transformSchema: (schema: GraphQLSchema) => upperDirectiveTransformer(schema, 'pe'),
            // installSubscriptionHandlers: true, 
            // playground: true, 
            // sortSchema: true,
            // buildSchemaOptions: {
            //    noDuplicatedFields: true,
            //   directives: [
            //     new GraphQLDirective({
            //       name: 'pe',
            //       locations: [DirectiveLocation.SCHEMA],
            //     }),
            //   ],
            // },
        }),
    ],
  })
  export class GQLModule {}