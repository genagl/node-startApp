import { CreatePEDto } from '@common/dto/create-pe.dto';
import { MetaFilter } from '@common/dto/meta-filter.dto';
import { POST_STATUS } from '@common/interface';
import { Injectable } from "@nestjs/common";
import { DeepPartial, FindOptionsWhere, Repository, SelectQueryBuilder } from 'typeorm';
import { PEEntity } from "../../entities/pe.entity";
import { IPaging, ORDER, TAXONOMY_FILTER } from "../../interface/paging";
import { IPERepository } from './i.pe.repository';

@Injectable()
export abstract class PERepository<T extends PEEntity, K extends CreatePEDto> implements IPERepository<T, K> { 
    private _repository: Repository<T>; 
    private _type:string;

    protected constructor ( _repository: Repository<T>, _type:string ) {
        this._type = _type  || "post"
        this._repository = _repository
    }

    getType = () => {
        return this._type
    }
     
    async find(dto: any, land_id?: number): Promise<T> {
        return this._repository.findOne(dto);
    }

    async findById( id: any, land_id?: number ) : Promise<T> {
        const options: FindOptionsWhere<T> = { id, };
        let element = await this._repository.findOneBy(options);
        element = {
            ...element,
            post_content: element.content,
            land_id,
            post_date: element.post_date || new Date(),
            logotype: element.icon,
        };
        return element; 
    }

    async findByIds( ids: any[], land_id?: number ) : Promise<T[]> { 
        return this._repository.findByIds(ids);
    }

    create(dto: DeepPartial<T>, land_id?: number) : Promise<T> {
        dto = {
            ...dto,
            land_id,
            name: dto.title,
            post_content: dto.content,
            thumbnail_name: dto.thumbnail_name || dto.thumbnail,
            post_date: dto.post_date || new Date(),
        }
        return this._repository.save(dto);
    }

    async update(id:number, dto: any, land_id?: number): Promise<T> {
        let item = await this.findById(id);
        await this._repository.update(id, {...item, ...dto});
        const element = await  this.findById(id);
        return {
            ...element, 
            ID: element.id.toString(),            
            logotype: element.icon,
        };
    }
    
    async findAll( findManyOptions?: any, land_id?: number ): Promise<T[]> {
        const qb = this._repository.createQueryBuilder(this._type); 
        const many = await qb.getMany();
        const elements = many.map(e => ({...e, ID: e.id.toString(), logotype: e.icon, }));
        return elements;
    }
    async findFilter( filter: IPaging = {}, land_id?: number ): Promise<T[]> {
        let qb = this._repository.createQueryBuilder(this._type);
        
        if( filter.offset ) {
            qb.offset( filter.offset );
        }

        if( filter.count && filter.count >= 0) {
            qb.limit( filter.count );
        }
        
        let where : any[] = [], 
            order : any = {
                [ `${this._type}.id` ]:     ORDER.ASC, 
                [ `${this._type}.title` ]:  ORDER.ASC, 
            },
            search: string; 

        if( land_id ) {
            where.push( land_id );
            qb.where( `land_id = :land_id`, {land_id} ); 
        }
        if( filter.order ) { 
            order[ `${this._type}.id` ]       = filter.order;
            order[ `${this._type}.title` ]    = filter.order;
        } 
 
        if( filter.search ) {
            search = 'title LIKE :search OR content LINE :search ';
            where.push( search );
            if( !where.length ) {
                qb.where( search, { search:filter.search } );
            }
            else {
                qb.andWhere( search, { search:filter.search } );
            }
        }

        if(filter.post_author) {
            where.push( filter.post_author ); 
            if( !where.length ) {
                qb.where( 'author_id = :author_id', { author_id:filter.post_author } );
            }
            else {
                qb.andWhere( 'author_id = :author_id', { author_id:filter.post_author } );
            }
        }

        if(filter.post_status && filter.post_status !== POST_STATUS.all ) {
            where.push(filter.post_status);
            if( !where.length ) {
                qb.where( 'post_status = :post_status', { post_status: filter.post_status } );
            }
            else {
                qb.andWhere( 'post_status = :post_status', { post_status: filter.post_status } );
            }
        }
        /*
            meta fields
        */
        if( filter.metas && Array.isArray(filter.metas)) { 
            
            // TODO:: only meta_relation = "AND"
             filter.metas.forEach(meta => {
                this.getQBMetas(meta, qb);
            });
        } 
        /*
            taxonomies
        */
        if(filter.taxonomies) {
            where.push(filter.taxonomies);
            filter.taxonomies.forEach((filter:TAXONOMY_FILTER) => {
                try {
                    qb.andWhere(`${filter.tax_name} IN (:...ids)`, {ids: filter.term_ids.map(id => Number(id))});
                }
                catch(e) {}
            });
        }
        
        qb.orderBy( order );
        // console.log(qb.expressionMap.wheres);
        const many = await this.getMany(qb);
        const elements = many.map(e => ({...e, ID: e.id.toString(), post_content: e.content, logotype: e.icon, }));
        return elements;
    }

    async getMany(qb: SelectQueryBuilder<T>, filter: IPaging = {} ) {
        return await qb.getMany();
    }

    async remove( ids: number, land_id?: number ) : Promise<T> {
        const idsArray: any[] = [ids];
        const qb = this._repository.createQueryBuilder(this._type);
        qb.where('id IN (:...ids)', {
            ids: idsArray, 
        });
        qb.softDelete().execute();
        return 
    }
    
    async delete( ids:string, land_id?: number ) {
        const idsArray = ids.split(',');
        const qb = this._repository.createQueryBuilder(this._type);
        qb.where('id IN (:...ids)', {
            ids: idsArray,
        });
        return qb.delete().execute();
    }

    private async getQBMetas(meta: MetaFilter, qb: SelectQueryBuilder<T> ) {
        try { 
            switch(meta.compare) {
                case "NOT EXISTS":
                    qb.andWhere( `${meta.key} IS NULL OR ${meta.key} = :value`, {value: ""} );
                    break;
                case "EXISTS": 
                    qb.andWhere( `${meta.key} IS NOT NULL  OR ${meta.key} != :value`, {value: ""}  );
                    break;
                case "IN":
                    if(Array.isArray(meta.target)) {
                        qb.andWhere( `${meta.key} IS IN :value`, {value: meta.target});
                    }
                    break;
                case "NOT IN":
                    if(Array.isArray(meta.target)) {
                        qb.andWhere( `${meta.key} IS NOT IN :value`, {value: meta.target});
                    }
                    break;
                case "BETWEEN":
                    if(Array.isArray(meta.target) && meta.target.length > 1) {
                        qb.andWhere( 
                            `${meta.key} BETWEEN :value1 AND :value2`,
                            {
                                value1: meta.target[0],
                                value2: meta.target[1],
                            }
                        );
                    }
                    break;
                case "NOT BETWEEN":
                    if(Array.isArray(meta.target) && meta.target.length > 1) {
                        qb.andWhere( 
                            `${meta.key} NOT BETWEEN :value1 AND :value2`,
                            {
                                value1: meta.target[0],
                                value2: meta.target[1],
                            }
                        );
                    }
                    break;
                case "=":
                default:
                   if(meta.value) {
                        qb.andWhere(`${meta.key} = :value`, {value: meta.value});
                    }
                    else if(meta.value_numeric) {
                        qb.andWhere(`${meta.key} = :value`, {value: meta.value_numeric});
                    }
                    else if(meta.value_bool ) {
                        qb.andWhere(`${meta.key} = :value`, {value: !!meta.value_bool});
                    } 
            }
            return qb;
        }
        catch( e ) {
            // console.error(e);
            return qb;
        }
    }
}

