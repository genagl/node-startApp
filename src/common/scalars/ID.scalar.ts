import { CustomScalar, Scalar } from '@nestjs/graphql';
import { Kind, ValueNode } from 'graphql';

@Scalar( 'ID' )
export class ID implements CustomScalar<string | number, string | number> {
  description = 'uniq Resource identifier ';

  parseValue(value: string | number): string | number {
    return value; // value from the client
  }

  serialize(value: string | number): string | number {
    return value; // value sent to the client
  }

  parseLiteral( ast: ValueNode ): string | number {
    if ( ast.kind === Kind.INT || ast.kind === Kind.STRING ) {
      return ast.value;
    }
    return null;
  }
}
