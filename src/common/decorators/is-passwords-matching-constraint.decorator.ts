import { CreateUserDto } from "@users/dto/create-user.dto";
import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";


@ValidatorConstraint({name: "isPasswordMatching", async: false})
export class IsPasswordsMatchingConstraint implements ValidatorConstraintInterface {
    validate(passwordRepeat: string, args: ValidationArguments) {
        const obj = args.object as CreateUserDto;
        return obj.password === passwordRepeat;
    }

    defaultMessage( ): string {
        return "пароли не совпадают";
    }
}