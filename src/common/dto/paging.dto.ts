import { TaxonomyFilter } from "@common/dto/TaxonomyFilter.dto";
import { MetaFilter } from "@common/dto/meta-filter.dto";
import { IPaging, POST_STATUS, TAXONOMY_FILTER } from "@common/interface";
import { Field, InputType, Int } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";


@InputType("Paging")
export default class PagingDto implements IPaging {
    
    @ApiProperty({default: 30})
    @Field(() => Int, { defaultValue:30, description: "Count of elements in list", nullable: true }) 
    @IsOptional()
    count?: number;

    @ApiProperty({default: 0})
    @Field(() => Int, { defaultValue:0,  description: "offset of list", nullable: true }) 
    @IsOptional()
    offset?: number;

    @ApiProperty({default: 0})
    @Field(() => String, { description: "visible status", nullable: true }) 
    @IsOptional() 
    post_status?: POST_STATUS;

    @ApiProperty({default: 0})
    @Field(() => String, { description: "order (ASC | DESC)", nullable: true }) 
    @IsOptional()
    order?: string;

    @ApiProperty( ) 
    @Field(() => String, { description: "Order by", nullable: true  }) 
    order_by_meta?: string; 

    @ApiProperty( ) 
    @Field(() => String, { description: "search", nullable: true  }) 
    search?: string;

    @ApiProperty( ) 
    @Field(() => [MetaFilter], { description: "MetaFilter", nullable: true  }) 
    metas?: MetaFilter[]; 

    @ApiProperty( ) 
    @Field(() => String, { description: "meta relation ( OR | AND)", nullable: true  }) 
    relation?: string;

    @ApiProperty( ) 
    @Field(() => String, { description: "taxonomy relation ( OR | AND)", nullable: true  }) 
    tax_relation?: string;

    @ApiProperty( ) 
    @Field(() => [TaxonomyFilter], { description: "taxonomy filter", nullable: true  }) 
    taxonomies?: TAXONOMY_FILTER[];
}