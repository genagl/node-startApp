import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { OptionsService } from './options.service';
import { OptionEntity } from './entities/option.entity';
import { CreateOptionInput } from './dto/create-option.input';
import { UpdateOptionInput } from './dto/update-option.input';

@Resolver(() => OptionEntity)
export class OptionsResolver {
  constructor(private readonly optionsService: OptionsService) {}

  @Mutation(() => OptionEntity, { name: `createOptions` })
  createOption(@Args('input') input: CreateOptionInput) {
    return this.optionsService.create(input);
  }
 
  @Query(() => OptionEntity, { name: 'getOptions' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.optionsService.findOne(id);
  } 

  @Mutation(() => OptionEntity, { name: `changeOptions` })
  updateOption(
    @Args('id', { type: () => Int }) id: number,
    @Args('input') input: UpdateOptionInput
  ) {
    return this.optionsService.update( id, input );
  }

  @Mutation(() => OptionEntity, { name: `deleteOptions` })
  removeOption(@Args('id', { type: () => Int }) id: number) {
    return this.optionsService.remove(id);
  }
}
