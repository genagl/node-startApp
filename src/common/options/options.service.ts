import { Inject, Injectable } from '@nestjs/common';
import { CreateOptionInput } from './dto/create-option.input';
import { UpdateOptionInput } from './dto/update-option.input'; 
import { OptionEntity } from './entities/option.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

@Injectable() 
export class OptionsService {
  constructor(
    @InjectRepository(OptionEntity)
    private repository: Repository<OptionEntity>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  async create(dto: CreateOptionInput) : Promise<OptionEntity> {
    const options: OptionEntity = await this.repository.save(dto);
    await this.cacheManager.set(options.id.toString(), options);
    await this.cacheManager.set(options.email, options);
    return options;
  }

  async findAll( ) : Promise<OptionEntity[]> {
    return await this.repository.find();
  }

  async findOne(id: number) : Promise<OptionEntity> {
    return await this.repository.findOneBy({ id, });
  }

  async update(id: number, dto: UpdateOptionInput) {
    const user = await this.repository.findOneBy({ id, });
    return await this.repository.update(id, {...user , ...dto});
  }

  async remove(id: number):Promise<any> {
    return await this.repository.delete({id});
  }
}
