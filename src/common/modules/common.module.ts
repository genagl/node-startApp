import { LandsModule } from "@lands/lands.module";
import { Module } from "@nestjs/common";
import { CommentModule } from "../comment/comment.module";
import { OptionsModule } from "../options/options.module";

@Module({
    imports: [       
        CommentModule,
        // OptionsModule,
        LandsModule,
    ], 
  })
  export class CommonModule {}