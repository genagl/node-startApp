import { IPaging, IPe } from "@common/interface"
import { DeepPartial } from "typeorm"

export interface IPEResolver<T extends IPe | unknown, K extends DeepPartial<T>> {
    getPE( id: number, land_id?:number, ): Promise<T>
    getAllPE( paging: IPaging, land_id?:number ): Promise<T[]>
    create( updateInput: K, land_id?:number, ): any
    update( id: number, updateInput: K, land_id?:number): any
    remove( id: number, land_id?:number ): Promise<number>
}