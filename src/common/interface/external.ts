import { registerEnumType } from "@nestjs/graphql";

export enum EXTERNAL_TYPE {
    VK="VK",
    TG="TG",
    YA="YA", 
    PE="PE", 
}

registerEnumType(EXTERNAL_TYPE, { name: 'EXTERNAL_TYPE' });

