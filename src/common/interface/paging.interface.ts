export interface IPaging {
    offset?: number
    count?: number
}