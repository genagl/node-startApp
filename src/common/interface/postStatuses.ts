import { registerEnumType } from "@nestjs/graphql";

export enum POST_STATUS {
    DRAFT="DRAFT",
    PUBLISH="PUBLISH", 
    all="all"
}

registerEnumType(POST_STATUS, {name: 'POST_STATUS'})