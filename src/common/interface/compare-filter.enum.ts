import { registerEnumType } from "@nestjs/graphql";

export enum COMPARE_FILTER {
    "EXISTS" = "EXISTS", 
    "NOT EXISTS" = "NOT EXISTS", 
    "IN" = "IN", 
    "NOT IN" = "NOT IN", 
    "BETWEEN" = "BETWEEN", 
    "NOT BETWEEN" = "NOT BETWEEN",
    "=" = "=", 
    "!=" = "!=", 
    ">" = ">", 
    "<" = "<", 
    ">=" = ">=", 
    "<=" = "<=", 
}

registerEnumType(COMPARE_FILTER, {
    name: 'COMPARE_FILTER',
});