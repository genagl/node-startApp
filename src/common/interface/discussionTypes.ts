import { registerEnumType } from "@nestjs/graphql";

export enum DISCUSSION_TYPES {
    POST= "POST",
    TAXONOMY= "TAXONOMY"
}

registerEnumType(DISCUSSION_TYPES, {
    name: 'DISCUSSION_TYPES',
  });