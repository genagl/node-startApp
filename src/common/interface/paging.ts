import { MetaFilter } from "@common/dto/meta-filter.dto"
import { ID } from "../scalars/ID.scalar"
import { POST_STATUS } from "./postStatuses"

export interface IPaging {
    offset?: number,
    count?: number
    parent?: ID
    post_status?: POST_STATUS,
    post_author?: ID, 
    meta_relation?: PAGING_RELATION,
    tax_relation?: string,
    order? : string,
    order_by_meta?: string
    metas?: MetaFilter[]
    search?: string
    is_admin?: boolean,
    relation?: string,
    taxonomies?: TAXONOMY_FILTER[],
}

export enum PAGING_RELATION {
    OR = "OR",
    AND ="AND"
}
export enum ORDER { 
    DESC="DESC",
    ASC="ASC"
}
export interface I_META_FILTER {
    key?: string,
    value?: string,
    value_numeric?: number,
    value_boolean?: boolean
    compare?: "=" | "!=" | ">" | "<" | ">=" | "<=" | "EXISTS" | "NOT EXISTS" | "IN" | "NOT IN" | "BETWEEN" | "NOT BETWEEN"
    target?: number | string | number[] | string[] | null
}
export interface TAXONOMY_FILTER {
    tax_name: string,
    term_ids: (number | string)[],
}