import { LandEntity } from "@src/lands/entities"
import { UserEntity } from "@src/users/entities/user.entity"
import { POST_STATUS } from "./postStatuses"

export interface HasId {
    id: number
}
export interface IPEBase { 
    title?: string 
    post_content?: string 
    password?: string 
    order?: number
    author_id?: number 
    author?: UserEntity  
    post_author?: UserEntity  
    post_status: POST_STATUS
    is_blocked?: boolean
    thumbnail?: string
    thumbnail_name?: string
    thumbnail_id?: number
    post_date?: Date | null 
}

export interface ILandes {
    land_id?: number | string
    land?: LandEntity
}

export type IPe = HasId & IPEBase & ILandes; 