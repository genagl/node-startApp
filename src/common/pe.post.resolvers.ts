import { Args, Int, Query, Mutation, Resolver } from '@nestjs/graphql';  
import { PostEntity } from 'src/modules/core/entities/post.entity';
import { PostService } from 'src/modules/core/post.service';
import { UserId } from 'src/users/decorators/user-id.decorator';

@Resolver( () => PostEntity )
export class FileResolver {
  constructor(private readonly postService: PostService) {}

  @Query( () => PostEntity, { name: 'getSingle',  nullable: true } )
  async getSinglePE(@Args('id', { type: () => Int }) id: number): Promise<PostEntity> {
    const file = await ( this.postService.findById( id ) ) 
    return file
  }

  @Mutation( () => PostEntity, {  name: 'removeSingle'} )
  removePE(@UserId() userId: number, ids: number) { 
    return this.postService.remove( ids );
  }

  @Mutation( () => PostEntity, {  name: 'deleteSingle'} )
  deletePE(@UserId() userId: number, ids: string) { 
    return this.postService.delete( ids );
  }
}
