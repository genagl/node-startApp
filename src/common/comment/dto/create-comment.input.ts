import { Field, ID, InputType, Int } from '@nestjs/graphql';
import { IsBoolean, IsInt, IsOptional, IsString } from 'class-validator';
import { DISCUSSION_TYPES, ID as _ID } from "../../interface";

@InputType()
export class CreateCommentInput { 
   
    @Field(() => ID, {description: "Discussion id"})  
    discussion_id: _ID;
      
    @IsString()
    @Field( { description: "Content" }) 
    content: string;
     
    @IsOptional() 
    @IsString()
    @Field(() => ID, {defaultValue: DISCUSSION_TYPES.POST, description: "Discussion type (post or taxonomy)", nullable: true  })  
    discussion_type?: DISCUSSION_TYPES | null; 
     
    @IsOptional()  
    @Field(() => ID, { defaultValue: 0, description: "Parent comment ID", nullable: true }) 
    parent?: _ID | null;
      
    @Field(() => ID, { description: "ID Author's of comment" })  
    author?: _ID; 
      
    @IsInt()  
    @Field(() => Int, { description: "UNIXtime int. Date of creation." })  
    date: number; 
     
    @IsOptional() 
    @IsBoolean()
    @Field(() => Boolean, {defaultValue:true, description: "Approve", nullable: true })
    is_approved?: boolean | null; 
}
