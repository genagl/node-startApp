import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentResolver } from './comment.resolver';
import { CommentController } from './comment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PECommentEntity } from './entities/pe.comment.entity';

@Module({
  providers: [ CommentService, CommentResolver ],
  controllers: [ CommentController ],
  imports: [ TypeOrmModule.forFeature([ PECommentEntity ]), ]
})
export class CommentModule {}
