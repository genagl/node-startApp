import { Field, ID, Int, ObjectType } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, ObjectLiteral, PrimaryGeneratedColumn } from "typeorm";
import { DISCUSSION_TYPES, ID as _ID } from "../../interface"
import { PEEntity } from "../../entities/pe.entity";
import { UserEntity } from "@src/users/entities/user.entity"; 

@Entity("comment")
@ObjectType( "Comment" ) 
export class PECommentEntity implements ObjectLiteral { 

    @PrimaryGeneratedColumn()
    @Field(() => ID, { nullable: true,  description: "Uniq identifier." })
    id: number;
    
    @Column("varchar", { nullable: true, default: "New site" }) 
    @Field(() => ID, {defaultValue: "New site", description: "Discussion id" }) 
    @ApiProperty({ example: "New site", description: 'Discussion id' , nullable: true  })
    discussion_id?: _ID | null;
    
    @Column("varchar", { nullable: true, default: "Content" }) 
    @Field(() => ID, { description: "Content"  }) 
    @ApiProperty({ nullable: true  })
    content?: string | null;
    
    @Column("varchar", { nullable: true, default: DISCUSSION_TYPES.POST }) 
    @Field(() => DISCUSSION_TYPES, {defaultValue: DISCUSSION_TYPES.POST, description: "Discussion type (post or taxonomy)" }) 
    @ApiProperty({ example: DISCUSSION_TYPES.POST, description: 'Discussion type (post or taxonomy)' , nullable: true  })
    discussion_type?: DISCUSSION_TYPES | null;
     
    @Field(() => PEEntity, { description: "Owner Discussion" }) 
    @ApiProperty({ type: PEEntity, description: 'Owner Discussion' , nullable: true  })
    discussion?: PEEntity | null;
     
    @Field(() => PECommentEntity, { description: "Parent comment." }) 
    @ApiProperty({ type: PECommentEntity, description: 'Parent comment.' , nullable: true  })
    parent?: PECommentEntity | null;
     
    @Field(() => UserEntity, { description: "Author of comment" }) 
    @ApiProperty({ type: UserEntity, description: 'Author of comment' , nullable: true  })
    author?: UserEntity | null; 
     
    @Field(() => Int, { description: "UNIXtime int. Date of creation." }) 
    @ApiProperty({ description: 'UNIXtime int. Date of creation.' , nullable: true  })
    date?: number | null; 
     
    @Field(() => Boolean, { description: "Approve" }) 
    @ApiProperty({ description: 'Approve' , nullable: true  })
    is_approved?: boolean | null; 
}