import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { CommentService } from './comment.service'; 
import { CreateCommentInput } from './dto/create-comment.input';
import { CommentInput } from './dto/update-comment.input';
import { PECommentEntity } from './entities/pe.comment.entity';

@Resolver(() => PECommentEntity)
export class CommentResolver {
  constructor(private readonly commentService: CommentService) {}

  @Mutation(() => PECommentEntity, { name: 'createComment', description: `Create single Comment` })
  createComment(@Args('createCommentInput') createCommentInput: CreateCommentInput) {
    return this.commentService.create(createCommentInput);
  }

  @Query(() => [PECommentEntity], { name: 'getComments', description: `Get list of Comment` })
  findAll() {
    return this.commentService.findAll();
  }

  @Query(() => PECommentEntity, { name: 'getComment', description: `Get single Comment` })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.commentService.findOne(id);
  }

  @Mutation(() => PECommentEntity, { name: 'changeComment', description: `Update single Comment` })
  updateComment(@Args('id', { type: () => Int }) id: number, @Args('commentInput') updateCommentInput: CommentInput) {
    return this.commentService.update(id, updateCommentInput);
  }

  @Mutation(() => PECommentEntity, { name: 'deleteComment', description: `Remove single Comment` })
  removeComment(@Args('id', { type: () => Int }) id: number) {
    return this.commentService.remove(id);
  }
}
