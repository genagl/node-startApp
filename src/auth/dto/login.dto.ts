import { Field, InputType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger'; 
import { IsEmail, IsString } from 'class-validator';

@InputType("TokenInput")
export class LoginDto {

  @ApiProperty({default: "test@test.ru"})
  // @IsEmail()
  @Field(()=> String, {name:"login"})
  login: string;
 
  @ApiProperty({default: "111"})
  @IsString()
  @Field(()=> String, {name:"password"})
  password: string;
  
  @ApiProperty({nullable: true})
  @Field(()=> String, {name:"grant_type"})
  grant_type?: string | null;
}
