import { CookieGql, UserAgentGql } from "@common/decorators";
import { Tokens } from "@common/interface";
import { convertToSecondsUtil } from "@common/utils";
import { BadRequestException, Body, UnauthorizedException, UseGuards, UseInterceptors } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Args, Context, Int, Mutation, Query, Resolver } from "@nestjs/graphql";
import { UserIdGql } from "@users/decorators";
import { UserEntity } from "@users/entities/user.entity";
import { UpdateURLAvatarInterceptor } from "@users/interceptors";
import { UserRolesNotNullableInterceptor } from "@users/interceptors/user-roles-not-nullable.interceptor";
import { UsersService } from "@users/users.service";
import { add } from "date-fns";
import { Request } from 'express';
import { REFRESH_TOKEN } from "./auth.controller";
import { AuthService } from "./auth.service";
import { LoginDto } from "./dto/login.dto";
import { TokensEntity } from "./dto/tokens.dto";
import { GraphqlAuthGuard } from "./guards/jwt.gql.guard";
import { UserResponse } from "@users/responses";
import { CreateUserDto } from "@users/dto/create-user.dto";

@Resolver(() => UserEntity)
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService,  
    private readonly configService: ConfigService,  
  ) {}

  @Mutation( () => TokensEntity, { name: 'token', description: "Log in user" } )  
  async login(
    @Args('input') loginDto: LoginDto,  
    @UserAgentGql() agent: string,
    @Context("req") req: Request
  )  { 
    const tokens: Tokens =  await this.authService.signIn( loginDto, agent );
    if (!tokens) {
      throw new BadRequestException(`Не получается войти с данными ${JSON.stringify( loginDto )}`);
    }
    req.res?.cookie(
      REFRESH_TOKEN, 
      tokens.refreshToken, 
      {
        httpOnly: true,
        sameSite: 'lax',
        expires: add( new Date(), { days: this.configService.get("REFRESH_EXPIRES_DAYS") } ),
        secure: this.configService.get('NODE_ENV', 'development') === 'production',
        path: '/',
      }
    );
    
    return {
      access_token: tokens.accessToken,
      refresh_token: "",
      token_type: "",
      expires_in: convertToSecondsUtil( this.configService.get( "EXPIRES_ACCESS" ) ),
    };
  }

  @Mutation(() => Boolean, { name: 'logout', description: "Log out user" }  )
  @UseGuards( GraphqlAuthGuard)
  async logout(
    @CookieGql(REFRESH_TOKEN) refreshToken: string, 
    @Context("req") req: Request,
  ) {
    if (!refreshToken) { 
      return true;
    }
    await this.authService.deleteRefreshToken(refreshToken);
    req.res?.cookie(REFRESH_TOKEN, '', { httpOnly: true, secure: true, expires: new Date() }); 
    return true;
  }

  @Mutation(() => Boolean, { name: 'refresh_token', description: "Refresh jwt-tokens" } )
  @UseGuards( GraphqlAuthGuard)
  async refresh_token(
    @CookieGql(REFRESH_TOKEN) refreshToken: string, 
    @Context("req") req: Request,
    @UserAgentGql() agent: string
  ) {
    if(!refreshToken) {
      throw new UnauthorizedException();
    }
    const tokens = await this.authService.refreshTokens(refreshToken, agent);
    if(!tokens) {
      throw new UnauthorizedException();
    }
    req.res?.cookie(
      REFRESH_TOKEN, 
      tokens.refreshToken, {
        httpOnly: true,
        sameSite: 'lax',
        expires: add( 
          new Date(), 
          { days: this.configService.get("REFRESH_EXPIRES_DAYS") } 
        ),
        secure: this.configService.get('NODE_ENV', 'development') === 'production',
        path: '/',
      }
    );
    return true;
  }
  
  @UseInterceptors(UpdateURLAvatarInterceptor)
  @UseInterceptors(UserRolesNotNullableInterceptor)
  @Query(() => UserEntity, { name: 'userInfo', description: "Get current User info" }) 
  @UseGuards( GraphqlAuthGuard )  
  async userInfo( 
    @Args('land_id', {type: () => Int, nullable: true }, ) land_id: number, 
    @UserIdGql() userId: number  
  ) {
    const user = await this.userService.findById( userId, land_id );
    return new UserResponse( user );
  }
 
  @Mutation(()=> UserEntity, {name: "register", description: "Sign up new User"})
  async register( @Args("input") dto: CreateUserDto) {
    console.log(dto);
    const user = await this.authService.register(dto);
    if(!user) {
      throw new BadRequestException(`Не получается зарегистрировать пользователя с данными ${JSON.stringify(dto)}`)
    }
    return new UserResponse( user );  
  }
}