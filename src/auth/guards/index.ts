import { JwtAuthGuard } from "./jwt.guard";
import { RolesGuard } from "./roles.guard";
import { GraphqlAuthGuard } from "./jwt.gql.guard"


export const GUARDS = [JwtAuthGuard, RolesGuard, GraphqlAuthGuard ];