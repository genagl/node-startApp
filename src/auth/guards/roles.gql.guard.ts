import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs"; 
import { Role } from "../../users/models/roles.enum"; 
import { GqlExecutionContext } from "@nestjs/graphql";

@Injectable()
export class GraphqlRolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;

    const requireRoles = this.reflector.getAllAndOverride<Role[]>(
        "roles",
        [
          ctx.getHandler(),
          ctx.getClass(),
        ]
    );
    // console.log(requireRoles);
    if (!requireRoles) {
      return true;
    } 
    const  user = req.user; 
    return requireRoles.some((role) => user.roles.includes(role));
  }
}