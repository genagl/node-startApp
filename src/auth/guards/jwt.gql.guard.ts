import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';

@Injectable() 
export class GraphqlAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  } 

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;
    const token = this.extractTokenFromHeader( req );
    if(!token) { 
        throw new UnauthorizedException( );
    }
    return req;
  } 

  private extractTokenFromHeader(request: Request): string | undefined {
    try {
      const [type, token] = request.headers["authorization"]?.split(' ') ?? [];
      return type === 'Bearer' ? token : undefined;
    }
    catch(err) {
      // throw new UnauthorizedException( );
    }
}
}