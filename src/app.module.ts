import { AuthModule } from '@auth/auth.module';
import { GQLModule } from '@common/graphql/graphql.module';
import { CommonModule } from '@common/modules/common.module';
import { PostgresDBModule } from '@common/modules/postgresdb.module';
import { LandsModule } from '@lands/lands.module';
import { CoreModule } from '@modules/core/core.module';
import { LudensModule } from '@modules/ludens/ludens.module';
import { ToposModule } from '@modules/topos/topos.module';
import { WebClientModule } from '@modules/web-client/web-client.module';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '@users/entities/user.entity';
import { UsersModule } from '@users/users.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileTermEntity } from './file-terms/entities/file-term.entity';
import { FileTermsModule } from './file-terms/file-terms.module';
import { FileEntity } from './files/entities/file.entity';
import { FilesModule } from './files/files.module';
import { FestModule } from './modules/fest/fest.module';

@Module({
  imports: [ 
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath:  [ '.env.local', '.env.development', '.env.product' ],
    }),
    GQLModule,
    PostgresDBModule,
    TypeOrmModule.forFeature([ UserEntity, FileEntity, FileTermEntity, ]),
    CacheModule.register({isGlobal: true}),
    LandsModule,
    CommonModule,
    WebClientModule,
    AuthModule,
    UsersModule,
    FilesModule,
    FileTermsModule,
    CoreModule,
    ToposModule,
    LudensModule,
    FestModule, 
  ],
  controllers: [ AppController, ],
  providers: [ AppService ],
})
export class AppModule {}
