import { Test, TestingModule } from '@nestjs/testing';
import { FileTermsService } from './file-terms.service';

describe('FileTermsService', () => {
  let service: FileTermsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FileTermsService],
    }).compile();

    service = module.get<FileTermsService>(FileTermsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
