import { PartialType } from '@nestjs/swagger';
import { CreateFileTermDto } from './create-file-term.dto';
import { InputType } from '@nestjs/graphql';

@InputType()
export class UpdateFileTermDto extends PartialType(CreateFileTermDto) {}
