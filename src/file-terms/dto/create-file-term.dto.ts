import { InputType } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

@InputType()
export class CreateFileTermDto {
    @ApiProperty({default: "Thumbnails"})
    @IsOptional() 
    @IsString()
    title?: string | null;
}
