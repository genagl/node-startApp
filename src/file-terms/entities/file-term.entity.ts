import { Field, ID, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';
import { FileEntity } from 'src/files/entities/file.entity';
import {
    Column, 
    Entity, 
    OneToMany,
    PrimaryGeneratedColumn,
  } from 'typeorm';


@Entity('fileTerms')
@ObjectType("FileTerm", { description: 'File Term' })
export class FileTermEntity {
    @PrimaryGeneratedColumn()
    @Field(() => ID)
    id: number;

    @Column()
    @MinLength(3)
    @MaxLength(50)
    @ApiProperty({ example: "Thumbnails", description: 'All images formats: jpg, png, bmp, svg ets.' })
    @Field(() => String) 
    title: string;
    
    /**/
    @Field( () => [FileEntity], { nullable: 'items' } )
    @OneToMany(() => FileEntity, (file) => file.term)
    files: FileEntity[]
    

}
