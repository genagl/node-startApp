import {
  Column,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UserEntity } from '../../users/entities/user.entity';
// import { FileTermEntity } from '../../file-terms/entities/file-term.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { FileTermEntity } from 'src/file-terms/entities/file-term.entity';

export enum FileType {
  PHOTOS = 'photos',
  TRASH = 'trash', 
}

@Entity('files')
@ObjectType("File", { description: 'File or image' })
export class FileEntity {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: number;

  @Column()
  @Field(() => String)
  @ApiProperty({ example: "Images", description: 'jpg, png, bmp, svg ets.' })
  filename: string;

  @Column()
  @Field(() => String)
  @ApiProperty({ example: "Original name", description: '---' })
  originalName: string;

  @Column()
  @Field(() => Int)
  @ApiProperty({ example: 230, description: 'kBt' })
  size: number;

  @Column()
  @Field(() => String)
  mimetype: string;

  /**/
  @Field(() => UserEntity )
  @ManyToOne(() => UserEntity, (user) => user.files)
  user: UserEntity; 

  @Field(() => FileTermEntity, {nullable:true})
  @ManyToOne(() => FileTermEntity, (term) => term.files )
  term?: FileTermEntity;
  

  @Column()
  @Field({nullable:true})
  @DeleteDateColumn()
  deletedAt?: Date;
}