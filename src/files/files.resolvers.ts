import { Args, Int, Query, Mutation, Resolver } from '@nestjs/graphql'; 
import { FileEntity, FileType } from './entities/file.entity';
import { FilesService } from './files.service';
import { UserId } from 'src/users/decorators/user-id.decorator';
import GraphQLJSON from 'graphql-type-json';

@Resolver( of => FileEntity )
export class FileResolver {
  constructor(private readonly fileService: FilesService) {}

  @Query( () => FileEntity, { name: 'getFile',  nullable: true, description: "Get single File" } )
  async getFile(@Args('id', { type: () => Int }) id: number): Promise<FileEntity> {
    const file = await ( this.fileService.findOne( id ) ) 
    return file
  }
  
  @Query(() => [FileEntity], { name: 'getFiles',  nullable: 'items', description: `Get list of Files` })
  async getFiles(@Args('id', { type: () => Int }) userId: number, fileType: FileType): Promise<FileEntity[]> {
    const files = await ( this.fileService.findAll( userId, fileType ) ) 
    return files
  }

  @Mutation( () => FileEntity, {  name: 'removeFile', description: `Soft remove single Comment`} )
  remove(@UserId() userId: number, ids: string) { 
    return this.fileService.remove(userId, ids);
  }

  @Mutation( () => GraphQLJSON, {  name: 'deleteFile', description: `Remove single Comment`} )
  delete(@UserId() userId: number, ids: string) { 
    return this.fileService.delete(userId, ids);
  }
}
