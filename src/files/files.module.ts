import { Module } from '@nestjs/common';
import { FilesService } from './files.service';
import { FilesController } from './files.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileResolver } from './files.resolvers';
import { FileEntity } from './entities/file.entity';

@Module({
  controllers: [FilesController],
  providers: [FilesService, FileResolver],
  imports: [TypeOrmModule.forFeature([FileEntity])],
})
export class FilesModule {}
