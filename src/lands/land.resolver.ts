import { PEResolver } from '@common/pe.resolver';
import { Resolver } from '@nestjs/graphql';
import { CreateLandDto } from './dto/create-land.dto';
import { LandEntity } from './entities';
import { LandService } from './land.service';

@Resolver(() => LandEntity)
export class LandResolver extends PEResolver(LandEntity, CreateLandDto, "Land") {
  constructor(private readonly landService: LandService) {
    super( landService );
  }
  
}