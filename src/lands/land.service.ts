import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { LandEntity } from "./entities/land.entity";
import { CreateLandDto } from "./dto/create-land.dto";

@Injectable()
export class LandService extends PERepository<LandEntity, CreateLandDto> {
    constructor( 
        @InjectRepository(LandEntity)
        private readonly _landRepository: Repository<LandEntity>,
    ) {
        super(_landRepository, "land");
    }
    
}