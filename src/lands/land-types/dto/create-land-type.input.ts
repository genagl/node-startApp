import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateLandTypeInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
