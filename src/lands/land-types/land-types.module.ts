import { Module } from '@nestjs/common';
import { LandTypesService } from './land-types.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LandTypeEntity } from './entities/land-type.entity';

@Module({
  providers: [ LandTypesService ], 
  imports: [ TypeOrmModule.forFeature([ LandTypeEntity ]) ],
})
export class LandTypesModule {}
