import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { LandTypeEntity } from './entities/land-type.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class LandTypesService {
    constructor( 
        @InjectRepository(LandTypeEntity) 
        private readonly _repository: Repository<LandTypeEntity>, 
    ) {}

    async findByName (name: string) {
        return this._repository.findBy({name});
    }

    async findById (id: number) {
        return this._repository.findBy({id});
    }

    async findAll ( ) {
        const qb = this._repository.createQueryBuilder("land-types");
        return qb.getMany();
    }

}
