import { CreatePEDto } from "@common/dto/create-pe.dto";
import { LAND_TYPES } from "@lands/land-types/interface";
import { Field, HideField, InputType } from '@nestjs/graphql';
import { UserLandEntity } from "@users/entities";
import { Role } from "@users/models/roles.enum";
import { IsEmail, IsPhoneNumber } from 'class-validator';
import { GraphQLJSONObject } from "graphql-type-json";

@InputType("LandInput")
export class CreateLandDto extends CreatePEDto { 

  @Field(() => String, {defaultValue: "New land", description: "Name of land", nullable: true  }) 
  name: string;
   
  @Field(() => String, {defaultValue: "", description: "Description of land", nullable: true  }) 
  description: string;
  
  @Field(() => String, {defaultValue: "", description: "Administrator email", nullable: true }) 
  @IsEmail()
  email: string;
  
  @Field(() => String, {defaultValue: "", description: "Public address", nullable: true }) 
  address: string;
  
  @Field(() => String, { description: "Help land URL", nullable: true }) 
  help_url: string;
  
  @Field(() => String, { description: "VK app ID", nullable: true }) 
  vk_app_id: string;
   
  @Field(() => String, { description: "VK link", nullable: true }) 
  vk: string;
     
  @Field(() => String, { description: "VK link 2", nullable: true })  
  vk2?: string | null;
   
  @Field(() => String, { description: "telegram link ", nullable: true })  
  telegramm?: string | null;
    
  @Field(() => String, { description: "telegram link 2", nullable: true })  
  telegramm2?: string | null;
     
  @Field(() => String, { description: "Contact phone", nullable: true }) 
  @IsPhoneNumber()
  phone?: string | null;
   
  @Field(() => String, { description: "Contact phone 2", nullable: true })  
  @IsPhoneNumber()
  phone2?: string | null;
  
  @Field(() => String, { description: "youtube link", nullable: true }) 
  youtube: string;
  
  @Field(() => String, { description: "Default image name", nullable: true }) 
  default_img_name: string;
   
  @Field(() => String, { description: "Default image ID", nullable: true }) 
  default_img_id: string;
  
  @Field(() => String, { description: "Default image", nullable: true }) 
  default_img: string; 
  
  @Field(() => Boolean, { description: "New User must e-mail activate account", nullable: true  })
  user_verify_account: boolean; 
  
  /* START ALIEN INJECTION */
  @Field(() => String, { description: "Default image ID", nullable: true }) 
  defaultThumbnail_id?: string | null;

  @Field(() => String, { description: "Default image", nullable: true }) 
  defaultThumbnail?: string | null;

  @Field(() => String, { description: "Default icon", nullable: true }) 
  icon?: string | null;

  @Field(() => String, { description: "Domain", nullable: true }) 
  domain?: string | null;

  @Field(() => String, { description: "Domain content", nullable: true }) 
  domain_content?: string | null;

  @Field(() => String, { description: "Domain description", nullable: true }) 
  domain_description?: string | null;
    
  @Field(() => Date, { description: "Start date", nullable: true }) 
  startDate?: Date | null;
    
  @Field(() => Date,  { description: "Finish date", nullable: true }) 
  finishDate?: Date | null;
    
  @Field(() => [String], { description: "Images gallery", nullable: true }) 
  images?: string[] | null;
    
  @Field(() => [String], { description: "Images gallery's names list. Use only for send names of new Images to server", nullable: true }) 
  images_names?: string[] | null;
    
  @Field(() => [String], { description: "Event types", nullable: true }) 
  event_types?: string[] | null;
    
  @Field(() => String, { description: "domain type", nullable: true }) 
  domain_type?: string | null;
    
  @Field(() => String,  { description: "Festival status", nullable: true }) 
  status?: string | null;

  @Field(() => Boolean, { description: "This Land is pattern for over new", nullable: true }) 
  isPattern?: boolean | null; 
    
  @Field(() => GraphQLJSONObject, { description: "Geo position", nullable: true }) 
  geo?: string[];
   
  @Field(() => [LAND_TYPES], { description: "Land type (show getLandTypes)", nullable: "itemsAndList" })  
  land_type?: LAND_TYPES[];

  @Field(() => GraphQLJSONObject, { description: "Land type (show getLandTypes)", nullable: true }) 
  land_data?: {};
  /* FINISH ALIEN INJECTION */ 
   
  @Field(() => [Role], { description: "List of available roles", nullable: true }) 
  availableRoles: Role[];
         
  @Field(() => String, { description: "URl of web-client", nullable: true })  
  clientUrl: string;
  
  @Field(() => String, { description: "External link 1", nullable: true }) 
  link1: string;
      
  @Field(() => String, { description: "External link 2", nullable: true })  
  link2: string; 
 
  @Field(() => Boolean, { description: "New user may register", nullable: true })  
  isRegisterUser?: boolean | false; 

  @Field(() => Boolean, { description: "Land reglament enabled", nullable: true })  
  enabledReglament?: boolean | false;
  
  @HideField()
  userLand: UserLandEntity[];
}