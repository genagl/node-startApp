import { Controller } from "@nestjs/common";
import { CreateTokenDto } from "./dto/create-token.dto";
import { TokenService } from "./token.service";

@Controller('tokens')  
export class TokenController {
    constructor( private readonly _tokenService: TokenService ){ }
     
    create( createUserDto: CreateTokenDto ) {
         
        return this._tokenService.create( createUserDto );
    }  
    
    update( id: number, dto: CreateTokenDto) {
        return this._tokenService.update( id, dto );
    }
 
    remove( id: number ) {
        return this._tokenService.remove( id.toString() );
    }
}