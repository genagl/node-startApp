import { IsString } from "class-validator"; 
export class CreateTokenDto {
      
    userId: number;

    @IsString()
    token: string;
 
    @IsString()
    expire: Date;

    @IsString()
    userAgent: string;
}