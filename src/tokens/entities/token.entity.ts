import { Column, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from "typeorm";  
import { IsString } from "class-validator";

@Entity("tokens")
export class TokenEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column() 
    userId: number

    @Column()  
    @IsString()
    token: string

    @Column()
    // @DeleteDateColumn()
    expire: Date

    @Column()  
    @IsString()
    userAgent: string
}