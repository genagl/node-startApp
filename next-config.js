const withSass = require("@zeit/next-sass")
const Dotenv = require('dotenv-webpack')

const nextConfig = {
  webpack: config => {
    config.plugins.push(
      new Dotenv({
        path: `./.env.${ process.env.NODE_ENV === 'production' ? 'production' : 'development' }`,
      }),
    )
    return config
  },
}

module.exports = withSass(nextConfig)